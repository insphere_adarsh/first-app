<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Register;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }


    /**
     *   Display Template page
     * 
     *  @return string
     */

    public function actionTemplate() {
        $model = new Register();   
       
        $model = $model->find()->all();
            


        return $this->render('template', array('data' => $model));
    }


    /**
      *   Function Performing insertion operation
      *   which modify register table
      */
    public function actionRegister(){

        
        $model = new Register();   
        if(isset($_POST['email'])) {
            
            $model->email = $_POST['email'];
            $model->name = isset($_POST['name']) ? $_POST['name']:'';
            $model->address = isset($_POST['address']) ? $_POST['address']:'';
            $model->phone_number = isset($_POST['phone_number']) ? $_POST['phone_number']:'';
            $model->gender = isset($_POST['gender']) ? $_POST['gender'] == '0'?'m':'f':'';
            $count=$model->save();

            if($count > 0){
                return "Your Record is inserted";
            }else{
                return "There is some problem while inserting record";
           }
        }
    }

    /**
      *  Function performing the Edit operation
      *  which modify regsiter table 
      */

    public function actionEdit(){

        $id = isset($_POST['editid'])?$_POST['editid']:'';
        $action =Yii::$app->db->createCommand( 'SELECT * FROM register where id='.$id.'')->queryOne();
        return json_encode($action);

    }

    /**
      *  Function performing the Update operation
      *  which modify regsiter table 
      */

    public function actionUpdate(){
            $email = isset($_POST['eemail']) ? $_POST['eemail']:'';
            $name = isset($_POST['ename']) ? $_POST['ename']:'';
            $address = isset($_POST['eaddress']) ? $_POST['eaddress']:'';
            $phone_number = isset($_POST['ephone_number']) ? $_POST['ephone_number']:'';
            $editid = isset($_POST['editid']) ? $_POST['editid']:'';
            $gender = isset($_POST['egender']) ? $_POST['egender'] == '0'?'m':'f':'';
           // $id = isset($_POST['id']);

          $count=Yii::$app->db->createCommand( 'update register set name=:name, address=:address, gender=:gender, phone_number=:phone_number  where id=:id')->bindValue(':id', $editid)
                  ->bindValue(':name', $name)
                  ->bindValue(':address', $address)
                  ->bindValue(':gender', $gender)
                  ->bindValue(':phone_number', $phone_number)
                  ->execute();

         if($count > 0){
            return "Record Updated Successfully";
         }else{
            return "There is some Problem while Updating record";
         }
        

    }

    /**
      *  Function performing the delete operation
      *  which modify regsiter table 
      */

    public function actionDelete(){
        $model = new Register();
        $id = $_POST['id'];
        $count=$model->findOne($id)->delete();

        if($count>0){
            return "Record Successfully deleted";
        }else{
            return "There is some problem while deleting Records";
        }
    }

}
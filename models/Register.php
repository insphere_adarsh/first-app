<?php
  
namespace app\models;

  class Register  extends\yii\db\ActiveRecord{

  	 /**  
     * @inheritdoc  
     */   
    public static function tableName()   
    {   
        return 'register';
    }


    public function rules()   
    {   
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            [
                'email', 'unique',
                'targetClass' => '\app\models\Register',         
                'message' => 'This email address has already been taken.'
            ]
        ];
    }   
  }   

?>
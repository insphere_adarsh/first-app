<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;   

$this->title = 'Template';
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
  .btn-xs, .btn-group-xs > .btn{
    padding: 4px 5px;
  }
</style>

<div class="container">
	<div class="row">
		
        
        <div class="col-md-12">
         <input type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal" value="Add">
          <div class="table-responsive">
             <table id="mytable" class="table">
                <thead>
                  <th style="display:none;">ID</th>
                  <th>User Name</th>
                  <th>Address</th>
                  <th>Email</th>
                  <th>Contact</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </thead>
               
                 <tbody>
                    <tr>
                      <?php
                        foreach($data as $row){
                          echo  "<tr>";
                          echo "<td style='display:none;'   width='20%'>".$row->id."</td>"; 
                          echo "<td width='20%'>".$row->name."</td>"; 
                          echo "<td width='20%'>".$row->address."</td>"; 
                          echo "<td>".$row->email."</td>"; 
                          echo "<td>".$row->phone_number."</td>"; 
                        ?>
                                <td width="2%"><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs editmodal" data-title="Edit" > <span class="glyphicon glyphicon-pencil"></span></button></p></td>
                        
                                <td width=2%><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs deletemodal" data-title="Delete" ><span class="glyphicon glyphicon-trash"></span></button></p></td>
                    <?php
                          echo "</tr>"; 
                        }

                      ?>
                    </tr>
               </tbody>
        </table>

        <div class="clearfix"></div>
        <ul class="pagination pull-right">
          <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
          <li class="active"><a href="#">1</a></li>
          <li><a href="#">2</a></li>
         
          <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
        </ul>
            </div>
        </div>
  	</div>
</div>
 
                     
            <!-- Edit Details -->
            <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            
                <div class="modal-dialog">
                  <form id="updateform">
                  <div class="modal-content">

                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                      </button>
                      <h4 class="modal-title custom_align" id="Heading">Edit Your Detail</h4>
                    </div>
                    <div class="modal-body">
                      <div class="form-group">
                        <input class="form-control " type="text" placeholder="User Name" id="ename" name="ename">
                          <input type="hidden" id="editid" name="editid">
                          </div>
                          <div class="form-group">
                            <input class="form-control" type="email" placeholder="Email" name="eemail" id="eemail" readonly>
                            </div>
                            <div class="form-group">
                              <input class="form-control " type="text" placeholder="Address" name="eaddress" id="eaddress">
                              </div>
                              <div class="form-group">
                                <select class="form-control" name="egender" id="egender">
                                  <option value="0">Male</option>
                                  <option value="1">Female</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <input class="form-control " type="number" placeholder="Phone Number" name="ephone_number" id="ephone_number">
                                </div>
                              </div>
                              <div class="modal-footer ">
                                <button type="button" class="btn btn-warning btn-lg" style="width: 100%;" onclick="Update()">
                                  <span class="glyphicon glyphicon-ok-sign" ></span> Update
                                </button>
                              </div>
                            </div>
                          </form>
                            <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                        </div>

    
          <!-- Deletre Model -->
          <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog">
          <div class="modal-content">
                <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
              <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
            </div>
                <div class="modal-body">
             <input type="hidden" id="id">
             <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
             
            </div>
              <div class="modal-footer ">
              <button type="button" class="btn btn-success" onclick="Delete()"><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
              <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
            </div>
              </div>
          <!-- /.modal-content --> 
        </div>
            <!-- /.modal-dialog --> 
          </div>


<div class="container">

  <!-- Modal -->
  <div class="modal fade" id="addModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <main class="my-form">
    <div class="cotainer">
        <div class="row justify-content-center">
            <div class="col-md-12 ">
                    <div class="card">
                        <div class="card-header"></div>
                        <div class="card-body">
                            <form name="my-form">
                                <div class="form-group row">
                                    <label for="full_name" class="col-md-4 col-form-label text-md-right">Name</label>
                                    <div class="col-md-8">
                                        <input type="text" id="name" class="form-control" name="name">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email_address" class="col-md-4 col-form-label text-md-right" >E-Mail Address</label>
                                    <div class="col-md-8">
                                        <input type="email" id="email" class="form-control" name="email" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="user_name" class="col-md-4 col-form-label text-md-right">Gender</label>
                                    <div class="col-md-8">
                                      <select name="gender" class="form-control">
                                         <option value="0">Male</option>
                                         <option value="1">Female</option>
                                      </select> 
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="phone_number" class="col-md-4 col-form-label text-md-right">Phone Number</label>
                                    <div class="col-md-8">
                                        <input type="number" id="phone_number" name="phone_number" class="form-control"  pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="permanent_address" class="col-md-4 col-form-label text-md-right">Address</label>
                                    <div class="col-md-8">
                                        <input type="text" id="address" class="form-control" name="address">
                                    </div>
                                </div>
                                </div>
                                <div class="modal-footer">

                                    <div class="col-md-2 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                        Register
                                        </button>
                                    </div>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                            </form>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</main>
        </div>
        
      </div>
      
    </div>
  </div>
  
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

 <script>


 	  $(function(){
        $('form').on('submit', function (e) {

          e.preventDefault();

          $.ajax({
            type: 'post',
            url: 'http://yiiapp.ngrock.com.io/web/index.php?r=site%2Fregister',
            data: $('form').serialize(),
            cache: false,
            success: function (data) {
              alert(data);
              location.reload();  
            }
          });
        });
    });


    var col_index;

    $(".deletemodal").on('click',function() {
      col_index = $(this).closest('tr').index();
 
      var id = $(this).closest('tr').children('td:eq(0)').text();
      $('#delete').modal('show');  
      $("#id").val(id);
});

    function Update(){
 
       $.ajax({
                type: 'post',
                url: 'http://yiiapp.ngrock.com.io/web/index.php?r=site%2Fupdate',
                data: $('#updateform').serialize(),
                cache: false,
                success: function (data) {
                  alert(data);
                  location.reload();  
                }
              });
      
    }

    $(".editmodal").on('click',function() {
        var id = $(this).closest('tr').children('td:eq(0)').text();
        $("#editid").val(id);
      
        $.ajax({
              type: 'post',
              url: 'http://yiiapp.ngrock.com.io/web/index.php?r=site%2Fedit',
              data: { editid:id },
              cache: false,
              success: function (data) {
                 var obj=JSON.parse(data);
                 $("#ename").val(obj.name);
                 $("#eemail").val(obj.email);
                 $("#eaddress").val(obj.address);
                 obj.gender=(obj.gender=='m')?'0':'1';
                 $("#egender").val(obj.gender);
                 $("#ephone_number").val(obj.phone_number);
              }
        });
     
        $('#edit').modal('show');  
    });


  function Delete(){
      var id= $("#id").val();
      $.ajax({
            type: 'post',
            url: 'http://yiiapp.ngrock.com.io/web/index.php?r=site%2Fdelete',
            data:{ id:id },
            cache: false,
            success: function (data) {
               location.reload();                 
         }
      });
  }

</script>